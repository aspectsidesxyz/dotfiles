#!/usr/bin/env bash
#            ██
#           ░██                ██   ██
#   ██████  ░██       ██████  ░░██ ██
#  ░░░░░░██ ░██████  ██░░░░██  ░░███
#   ███████ ░██░░░██░██   ░██   ░██
#  ██░░░░██ ░██  ░██░██   ░██   ██
# ░░████████░██  ░██░░██████   ██
#  ░░░░░░░░ ░░   ░░  ░░░░░░   ░░
#
#  author ▓▒ pyratebeard
#    code ▓▒ https://git.pyratebeard.net/
#
# TODO
# * add rkhunter check
# * add clamav check
# * server details

basename="${0##*/}"

# colours
black="\e[30m"
red="\e[31m"
green="\e[32m"
yellow="\e[33m"
blue="\e[34m"
magenta="\e[35m"
cyan="\e[36m"
white="\e[37m"
light_black="\e[1;30m"
light_red="\e[1;31m"
light_green="\e[1;32m"
light_yellow="\e[1;33m"
light_blue="\e[1;34m"
light_magenta="\e[1;35m"
light_cyan="\e[1;36m"
light_white="\e[1;37m"
lighter_black="\e[38;5;241m"
reset="\e[0m"

display_tasks=0

usage() {
	echo "Usage: ${basename} [-b] [-h] [-m] [-t] [-r] [-s <hostname>] [-w]

	-b: backups
	-h: help (this page)
	-m: mail
	-t: tasking
	-r: rss
	-s: show <hostname> details, use 'all' to show all
	-w: weather
"
}

heading() {
	header="$(echo ${1} | sed -e 's/\(.\)/\1 /g')"
	echo -e "${lighter_black}├┄ ${light_white}${header}${reset}"
}

tasking() {
	heading ${FUNCNAME[0]}
	total_tasks=$(cat $HOME/.todo | wc -l)

	echo -e "${lighter_black}│${reset}you have ${light_green}${total_tasks}${reset} task(s)"
}

# check nightly backups
backups() {
	heading ${FUNCNAME[0]}
	local backup_success=0
	local failed=""
	_repos=$(find /var/dedup/* -maxdepth 1 -type d -prune -printf '%f\n')
	for r in ${_repos} ; do
		_latest=$(find /var/dedup/${r}/archive -type f \
					-exec stat -c '%X %n' {} \; | \
					sort -nr | \
					awk -F\/ 'NR==1 {print $NF}')
		if [[ "${_latest}" -lt $(date +%Y%m%d) ]] ; then
			backup_success=$(( backup_success + 1 ))
			failed+="${r} "
		fi
	done
	[[ ${backup_success} -gt 0 ]] \
		&& echo -e "${lighter_black}│${reset}frak! ${failed}${red}failed${reset}" \
		|| echo -e "${lighter_black}│${reset}schway! backup ${light_green}successful${reset}"
}

# weather in current location
weather() {
	heading ${FUNCNAME[0]}
	if [ -f /tmp/weather ] ; then
		wttr=$(cat /tmp/weather | awk -F\: '{print $2}' | cut -c 2-)
		echo -e "${lighter_black}│${reset}${wttr}"
	else
		$HOME/bin/weather
	fi
}

# you've got mail
mail() {
	heading ${FUNCNAME[0]}
	local -r mail_dir="$HOME/var/mbox"

	local -r pyratebeard_mail=$(find "${mail_dir}"/pyratebeard/INBOX/new/ -type f | wc -l)
	local -r unread_mail=$(find "${mail_dir}"/pyratebeard/INBOX/cur/ -type f ! -name "*S" | wc -l)
	echo -e "${lighter_black}│${reset}you have ${light_green}${pyratebeard_mail}${reset} new mail(s)"
	echo -e "${lighter_black}│${reset}you have ${light_green}${unread_mail}${reset} unread mail(s)"
}

# not currently used but keeping for future reference
getnews() {
	local -r api_key=$(pass websites/newsapi/key)
	curl 'https://newsapi.org/v2/top-headlines' -s -G \
	-d sources="$1" \
	-d pageSize=5 \
	-d apiKey="${api_key}" | jq -r 'def default: "\u001b[0m"; def grey: "\u001b[1;30m"; .articles[] | .title, grey + .url, "" + default' | tr -d '\t'
}

rss() {
	heading ${FUNCNAME[0]}
	if [ -f ~/.newsboat/cache.db.lock ] ; then
		echo -e "${lighter_black}│${reset}${light_black}waiting for newsboat refresh...${reset}" 
		sleep 10
	fi
	local unread_count
	unread_count=$(sqlite3 ~/.newsboat/cache.db "select count(unread) from rss_item where unread = 1;")
	local queued_pods
	queued_pods=$(wc -l ~/.newsboat/queue | awk '{print $1}')
	echo -e "${lighter_black}│${reset}there are ${light_green}${unread_count}${reset} unread items"
	echo -e "${lighter_black}│${reset}you have ${light_green}${queued_pods}${reset} queued podcasts"
}

servers() {
	echo -e "${cyan}
    ┐─┐┬─┐┬─┐┐ ┬┬─┐┬─┐┐─┐
    └─┐├─ │┬┘│┌┘├─ │┬┘└─┐
    ──┘┴─┘┆└┘└┘ ┴─┘┆└┘──┘
${reset}"
	local server="$1"
	if [ "${server}" == "all" ] ; then
		for node in blacksun grimoire pyratetube mordhaus ; do
			heading "${node}"
			uptime=$(ssh -q "${node}" "uptime -p | cut -b4- | sed 's/\(,\|ear\|eek\(s\)\|ay\(s\)\|our\(s\)\|inute\(s\)\)//g'")
			echo -e "uptime is ${uptime}"
			echo
		done
	else
		heading "${server}"
		uptime=$(ssh -q "${server}" "uptime -p | cut -b4- | sed 's/\(,\|ear\|eek(s)\|ay(s)\|our(s)\|inute(s)\)//g'")

		echo -e "uptime is ${uptime}"
		echo
	fi
}

main() {
# 	echo -e "${blue}│${magenta} ┳━┓┳ ┳┏━┓┓ ┳
#${blue}│${magenta} ┃━┫┃━┫┃ ┃┗┏┛
#${blue}│${magenta} ┛ ┇┇ ┻┛━┛ ┇
#${blue}└──┄┄────┄┄───┐${yellow}
#  $(whoami) ${blue}│
#┌──┄┄────┄┄───┘${reset}"
#${lighter_black}┊ ${light_blue}██    ▄  █ ████▄ ▀▄    ▄ ▄
#${lighter_black}┊ ${light_blue}█ █  █   █ █   █   █  █ █
#${lighter_black}│ ${light_blue}▓▄▄█ ▓▓▀▀█ ▓   █    ▀█ █
#${lighter_black}┊ ${light_blue}▓  ▒ ▓   ▓ ▀▓▓▒▒    ▓  ▒
#${lighter_black}│ ${light_blue}   ▒    ▒         ▄▀
#${lighter_black}├───${light_blue}▒${lighter_black}────${light_blue}▀${lighter_black}┄┄ ${light_yellow}$(whoami)${light_blue} ▀
#${lighter_black}│ ${light_blue} ▀${reset}"
	echo -e "
${lighter_black}┊ ${light_blue} ██  █ █ ██  █ █ ${reset}${light_black}   ▄█████▄
${lighter_black}┊ ${light_blue} █ █ █ █ █ █ █ █ ${reset}${light_black}  ▄███████▄
${lighter_black}│ ${light_blue} ▓ █ ▓ █ ▓ █ ▓ █ ${reset}${light_black} ░██ ░█ ░██
${lighter_black}┊ ${light_blue} ▓▓▓ ▓▓▓ ▓ ▓  ▓  ${reset}${light_black} ░░███████
${lighter_black}│ ${light_blue} ▒ ▒ ▒ ▒ ▒▒▒  ▒  ${reset}${light_black}  ░░█░█░█
${lighter_black}├────${light_blue}▒${lighter_black}─${light_blue}▒${lighter_black}┄ ${light_yellow}$(whoami) ${reset}${light_black}░ ░ ░
${lighter_black}│    ${light_blue}▒ ▒${reset}"
#     ┳━┓┳ ┳┏━┓┓ ┳
#     ┃━┫┃━┫┃ ┃┗┏┛
#     ┛ ┇┇ ┻┛━┛ ┇
#      ┳━┓┳ ┳┏━┓o
#      ┃━┫┃━┫┃ ┃┃
#      ┛ ┇┇ ┻┛━┛┇
#	 かいぞく
# ░█▀█░█░█░█▀█░█░█░
# ░█▀█░█▀█░█░█░░█░░
# ░▀░▀░▀░▀░▀▀▀░░▀░░
#
	#echo -e "  greetings, ${white}\033[7m $(whoami) ${reset}\n"

	tasking
	echo -e "${lighter_black}│${reset}"
	backups
	echo -e "${lighter_black}│${reset}"
	weather
	echo -e "${lighter_black}│${reset}"
	mail
	echo -e "${lighter_black}│${reset}"
	rss
	echo -e "${lighter_black}└──┄┄────┄┄${reset}"
}

if [ $# -eq 0 ] ; then
	main
else
	while getopts ":bhmtrws:" opt ; do
		case ${opt} in
			b) backups ;;
			h) usage ;;
			m) mail ;;
			t) display_tasks=1 ; tasking ;;
			r) rss ;;
			w) weather ;;
			s) servers "${OPTARG}" ;;
			*) usage ;;
		esac
	done
fi
