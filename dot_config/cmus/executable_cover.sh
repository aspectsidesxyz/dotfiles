#!/usr/bin/env sh

#-------------------------------#
# Display current cover         #
#-------------------------------#

# Specify image display method
# Can be: chafa, urxvt, or ueberzug
METHOD="chafa"

draw() {
    case "$METHOD" in
        ueberzug)
	        ## Ueberzug method
	        printf '{"action": "add", "identifier": "PREVIEW", "x": "%s", "y": "%s", "width": "%s", "height": "%s", "scaler": "contain", "path": "%s"}\n' "$4" "$5" "$(($2 - 1))" "$(($3 - 1))" "$1" >"$FIFO_UEBERZUG"
            ;;
        urxvt)
	        ## URxvt method
	        printf "\ePtmux;\e\e]20;%s;40x40+96+11:op=keep-aspect\a\e\\" "$1"
            ;;
    esac
}

cleanup() {
    case "$METHOD" in
        ueberzug)
	        ## Ueberzug method
	        exec 3>&-
	        rm "$FIFO_UEBERZUG"
            ;;
        urxvt)
	        ## URxvt method
	        printf "\ePtmux;\e\e]20;;100x100+1000+1000\a\e\\"
            ;;
    esac
}

## Ueberzug method
if [ -n "$DISPLAY" ] && [ "$METHOD" = "ueberzug" ]; then
	export FIFO_UEBERZUG="${TMPDIR:-/tmp}/cmus-ueberzug-$$"

	mkfifo "$FIFO_UEBERZUG"
	ueberzug layer -s -p json <"$FIFO_UEBERZUG" &
	exec 3>"$FIFO_UEBERZUG"
	trap cleanup EXIT
fi

clear
[ -e /tmp/cmus_cover.fifo ] && rm /tmp/cmus_cover.fifo
mkfifo /tmp/cmus_cover.fifo
tail -f /tmp/cmus_cover.fifo |
	while read -r event; do
		[ "$event" = "clear" ] && cleanup
		art=$(mktemp --suffix ".png")
		filepath=$(cmus-remote -C "format_print %f")
		ffmpeg -nostdin -y -i "$filepath" "$art" >/dev/null 2>&1
		clear
		pad=""
		for i in $(seq 8 $(tput cols)); do
			pad=" $pad"
		done
		printf "%b" "\033[1m\033[31m\033[40mArtwork${pad}\033[0m\n"
		case "$METHOD" in
            urxvt)
			    ## URxvt method
			    cleanup
			    draw "$art" && sleep 0.5 && rm "$art"
                ;;
		    ueberzug)
			    ## Ueberzug method
			    draw "$art" "$(tput cols)" "$(tput lines)" 1 2
                ;;
		    chafa)
			    ## Chafa method
			    printf "%b" "\n"
			    chafa -c full "${art}" 2>/dev/null
			    rm "$art"
                ;;
        esac
	done
