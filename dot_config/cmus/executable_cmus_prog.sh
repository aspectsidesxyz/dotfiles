#!/bin/sh

# Print a progress bar for current track in cmus

# Chars to represent each state
elapsed='━'
#indc='■'
indc='━'
bar='━'

PBLK="\033[30m"
RED="\033[31m"
PUR="\033[35m"
GRY="\033[90;1m"

tput civis
while :; do
    # Do a quick check rather redrawing every single time
    if [ "$(cmus-remote -Q | head -1)" != "status playing" ]; then
        sleep 1
        continue
    fi

    # pos/dur in seconds
    pos=$(cmus-remote -C "format_print %{position}" | awk -F: '{ print ($1 * 60) + $2 }')
    dur=$(cmus-remote -C "format_print %d" | awk -F: '{ print ($1 * 60) + $2 }')
    cols=$(tput cols)

    # right as in right side of equation: dur * x = pos * cols
    right=$((pos * cols))
    total_cols=$(echo "$right/$dur" | bc -l | awk '{printf("%d\n",$1 + 0.5)}')
    elapse_cols=$((total_cols - 1))
    test "$elapse_cols" -ne -1 && bar_cols=$((cols - total_cols)) || bar_cols=$((cols - 1))

    # Move cursor back to start
    printf '\r'
    for b in $(seq $elapse_cols); do
        printf "%b" "${PUR}${elapsed}"
    done

    # Indicator
    printf "%b" "${PUR}$indc"

    for b in $(seq $bar_cols); do
        printf "%b" "${PBLK}$bar"
    done
    sleep 1
done
