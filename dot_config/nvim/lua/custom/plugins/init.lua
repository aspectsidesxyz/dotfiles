-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- See the kickstart.nvim README for more information
return {
    { 'echasnovski/mini.nvim', version = false,
        config = function ()
            require('mini.starter').setup(
            {
              autoopen = true,
              evaluate_single = false,
              items = nil,
              header = nil,
              footer = 'I saw a solar flare today',
              content_hooks = nil,
              query_updaters = 'abcdefghijklmnopqrstuvwxyz0123456789_-.',
            }
            )
        end
    },
    { 'AlessandroYorba/Alduin' },
    { 'mbbill/undotree'},
    {
        'norcalli/nvim-colorizer.lua',
        config = function ()
            require'colorizer'.setup()
        end
    },
    {
        'christoomey/vim-tmux-navigator'
    },
{
  "Everblush/nvim", name = "everblush"
},
    {
        'decaycs/decay.nvim', name = "decay"
    },
{
	'ThePrimeagen/harpoon',
	dependencies = 'nvim-lua/plenary.nvim',
    },
{
        'terrortylor/nvim-comment',
        config = function ()
            require('nvim_comment').setup()
        end
    },
{
	'kylechui/nvim-surround',
    config = function()
        require("nvim-surround").setup({
            -- Configuration here, or leave empty to use defaults
        })
    end
    },
{ 'nvim-tree/nvim-tree.lua',
    dependencies = {
    'nvim-tree/nvim-web-devicons', -- optional, for file icons
    tag = 'nightly',
    config = function() require'nvim-tree'.setup {} end
  },
  },
 {
"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
 },
}


