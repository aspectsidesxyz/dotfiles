#!/bin/sh

# Get the number of available updates
updates=$(checkupdates 2>/dev/null | wc -l)

# Set the icon
icon="  "

# Format the output for Waybar
output="{ \"text\": \"$icon $updates\", \"tooltip\": \"Updates available\", \"class\": \"updates\" }"

# Print the output as JSON
echo $output
