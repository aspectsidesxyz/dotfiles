#!/usr/bin/env bash

# Define the command to run when the module is clicked
CMD="rofi-bluetooth"

# Check if Bluetooth is enabled
if [[ $(bluetoothctl show | grep Powered | awk '{print $2}') == "no" ]]; then
  echo -e "{\"text\":\"\",\"tooltip\":\"Bluetooth is disabled\",\"class\":\"disabled\"}"
  exit 0
fi

# Get the name of the connected device, if any
device_name=$(bluetoothctl info | grep "Device Name" | awk '{print $3}')
if [[ $(bluetoothctl info | grep Connected | awk '{print $2}') == "yes" ]]; then
  echo -e "{\"text\":\" $device_name\",\"tooltip\":\"Bluetooth is connected to $device_name\",\"class\":\"connected\"}"
else
  echo -e "{\"text\":\"\",\"tooltip\":\"Bluetooth is disconnected\",\"class\":\"disconnected\"}"
fi

# When the module is clicked, execute the defined command
case "$BLOCK_BUTTON" in
  1) $CMD ;;
esac
